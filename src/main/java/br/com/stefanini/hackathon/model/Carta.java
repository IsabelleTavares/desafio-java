package br.com.stefanini.hackathon.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.OneToOne;



@Entity
public class Carta {
	
	@Id
	@GeneratedValue
	private Long id;
	
		
	@Column
	private String mensagem;
	
	@OneToOne
	@JoinColumn (name = "id")
	private Pessoa destinatario;
	
	@OneToOne
	@JoinColumn (name = "id")
	private Pessoa remetente;
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}	

	public String getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
   
 
    	
    
	
}
